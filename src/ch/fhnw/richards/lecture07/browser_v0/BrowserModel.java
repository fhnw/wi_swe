package ch.fhnw.richards.lecture07.browser_v0;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class BrowserModel {
    public String browse(String ipAddress, Integer port) {
        String lineIn;
        StringBuffer urlContent = new StringBuffer();

        // Set up the socket and the input/output streams
        // Network errors are always possible - therefore try/catch
        try ( Socket s = new Socket(ipAddress, port);
        		OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
        		BufferedReader inReader = new BufferedReader(new InputStreamReader(s.getInputStream()));        		
        		) {
            // Send our request, using the HTTP 1.0 protocol
            // Note: HTTP specifies \r\n line endings, although most programs don't care
            out.write("GET / HTTP/1.0\r\n");
            out.write("User-Agent: Browser0\r\n");
            out.write("Host: " + ipAddress + ":" + port + "\r\n");
            out.write("Accept: text/html, */*\r\n\r\n");
            out.flush();

            // Read the reply into a StringBuffer
            while ((lineIn = inReader.readLine()) != null) {
                urlContent.append(lineIn + "\n");
            }
        }

        // If an error occurred, add the error message to the String Buffer
        catch (Exception err) {
            urlContent.append("ERROR: " + err.toString());
        }
        
        return urlContent.toString();
    }
}
